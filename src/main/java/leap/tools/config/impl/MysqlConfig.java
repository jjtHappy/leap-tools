package leap.tools.config.impl;

import leap.tools.config.JDBCConfig;

/**
 * Title:
 * Description:
 * @author jjtEatJava
 * @date 2017年8月8日
 */
public class MysqlConfig implements JDBCConfig {
	private String driver = "com.mysql.jdbc.Driver";
	private String url;
	private String username;
	private String password;
	

	
	public MysqlConfig(String url,String username,String passwrod) {
		this.url=url;
		this.username=username;
		this.password=passwrod;
	}

	/* （非 Javadoc）
	 * @see com.kiki.jjt.freemark.config.JDBCConfig#getDriver()
	 */
	@Override
	public String getDriver() {
		return this.driver;
	}

	/* （非 Javadoc）
	 * @see com.kiki.jjt.freemark.config.JDBCConfig#getUrl()
	 */
	@Override
	public String getUrl() {
		return this.url;
	}

	/* （非 Javadoc）
	 * @see com.kiki.jjt.freemark.config.JDBCConfig#getUsername()
	 */
	@Override
	public String getUsername() {
		return this.username;
	}

	/* （非 Javadoc）
	 * @see com.kiki.jjt.freemark.config.JDBCConfig#getPassword()
	 */
	@Override
	public String getPassword() {
		return this.password;
	}

}
