package leap.tools.config;

/**
 * Title:
 * Description:
 * @author jjtEatJava
 * @date 2017年8月8日
 */
public interface JDBCConfig extends Config{
	String getDriver();
	String getUrl();
	String getUsername();
	String getPassword();
}
