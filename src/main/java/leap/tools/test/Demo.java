package leap.tools.test;

import java.io.IOException;
import java.sql.SQLException;

import freemarker.core.ParseException;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;
import leap.tools.builder.BeanBuilder;
import leap.tools.builder.ServiceBuilder;
import leap.tools.config.impl.MysqlConfig;

/**
 * Title:测试用例
 * Description:
 * @author jjtEatJava
 * @date 2017年8月8日
 */
public class Demo {
	public static void main(String[] args) throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, TemplateException, SQLException {
		MysqlConfig conf = new MysqlConfig("jdbc:mysql://localhost:3306/leap-tool-test", "root", "");
		BeanBuilder builder = new BeanBuilder(conf, "H:\\test4", "leap.tool.test","_");//生成bean
		ServiceBuilder builder2 = new ServiceBuilder(conf, "H:\\test4", "leap.tool.test","_");//默认使用一个主键的类型进行封装
		builder.build();
		builder2.build();
	}
}
