/**
 * Title:
 * Description:
 * @author jjtEatJava
 * @date 2017年7月28日
 */
package leap.tools.builder;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import freemarker.core.ParseException;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;
import leap.tools.bean.Field;
import leap.tools.config.JDBCConfig;
import leap.tools.logic.SimpleService;
import leap.tools.util.CamelChangeUtil;
import leap.tools.util.ReadDirectory;

/**
 * Title: Description:
 * 
 * @author jjtEatJava
 * @date 2017年7月28日
 */
public class ServiceBuilder extends Builder {
	private String packageName;
	private String basePath;
	private SimpleService service;
	private String mod;
	/**
	 * 
	 * Title:
	 * Description:
	 * @param jdbcConfig JDBC配置
	 * @param basePath 生成路径
	 * @param packageName 报名
	 * @param mod 切割字符
	 * @author jjtEatJava
	 * @date 2017年8月8日
	 */
	public ServiceBuilder(JDBCConfig jdbcConfig,String basePath,String packageName,String mod) {
		this.packageName=packageName;
		this.basePath=new File(basePath,"leap").getAbsolutePath();
		this.mod=mod;
		service = new SimpleService(jdbcConfig);
	}

	/*
	 * （非 Javadoc）
	 * 
	 * @see com.kiki.jjt.freemark.Builder#build()
	 */
	@Override
	public boolean build() throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, TemplateException, SQLException {
		//配置包名
		FreeMarkService.getCfg().setSharedVariable("packageName", packageName);
		//构建BaseService
		//文件名
		String baseServiceFileName = "BaseService.java";
		//模版名
		String baseServiceTempName = "BaseService.ftl";
		//数据模型
		Map<String, Object> baseServiceModel = new HashMap<String, Object>();
		baseServiceModel.put("packageName", packageName);
		work(baseServiceTempName, baseServiceModel,new File(basePath,"service").getAbsolutePath(), baseServiceFileName);

		// 构建BaseServiceImpl
		//文件名
		String baseServiceImplFileName = "BaseServiceImpl.java";
		//模版名
		String baseServiceImplTempName = "BaseServiceImpl.ftl";
		//数据模型
		Map<String, Object> baseServiceImplModel = new HashMap<String, Object>();
		baseServiceImplModel.put("packageName", packageName);
		work(baseServiceImplTempName, baseServiceImplModel, new File(new File(basePath,"service"),"impl").getAbsolutePath(), baseServiceImplFileName);

		//构建所有相关的实体Service
		//模版名
		String interfaceT = "EntityService.ftl";
		String implT = "EntityServiceImpl.ftl";
		//获取所有表名
		List<String> tableNames = service.getTablesName();
		//构建数据模型列表
		Map<String, Object> model = new HashMap<String, Object>();
		for (String tableName : tableNames) {
			model.put("entityName", CamelChangeUtil.toCamel(tableName,mod));
			model.put("priType",getPriType(tableName));
			// 创建接口文件
			work(interfaceT, model, new File(basePath,"service").getAbsolutePath(), CamelChangeUtil.toCamel(tableName,mod) + "Service.java");
			// 创建实现文件
			work(implT, model,new File(new File(basePath,"service"),"impl").getAbsolutePath(), CamelChangeUtil.toCamel(tableName,mod) + "ServiceImpl.java");
		}
		//打印树形结构目录
		  ReadDirectory rd = new ReadDirectory();
          rd.printDir(basePath);
          rd.readFile(basePath);
          return true;
	}

	/**
	 * Title:
	 * Description:
	 * @param tableName
	 * @return
	 * @author jjtEatJava
	 * @throws SQLException 
	 * @date 2017年8月8日
	 */
	private Object getPriType(String tableName) throws SQLException {
		List<Field> fields = service.getFieldsByTableName(tableName, mod);
		for(Field field:fields)
			if(field.getTableKey().contains("PRI"))
				return field.getJavaType();
		throw new IllegalArgumentException("数据库表："+tableName+" 没有主键");
	}
}
