package leap.tools.builder;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.io.Writer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.sql.SQLException;
import java.util.Map;
import freemarker.core.ParseException;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;
/**
 * Title:
 * Description:
 * @author jjtEatJava
 * @date 2017年7月30日
 */
public abstract class Builder {
	
	/**
	 * 
	 * Title:开始执行工作
	 * Description:
	 * @param tempName 模版名
	 * @param model 数据模型
	 * @param basePath 基础文件路径
	 * @param fileName 文件名
	 * @throws TemplateNotFoundException
	 * @throws MalformedTemplateNameException
	 * @throws ParseException
	 * @throws IOException
	 * @throws TemplateException
	 * @author jjtEatJava
	 * @date 2017年8月8日
	 */
	 protected  void work(String tempName,Map<String,Object> model,String basePath,String fileName) throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, TemplateException {
		 //获取模版
		Template temp = FreeMarkService.getCfg().getTemplate(tempName);
		//创建文件
		createFile(temp, model,basePath,fileName);
	}

	 /**
	  * 
	  * Title:创建文件
	  * Description:
	  * @param temp 模版
	  * @param model 数据模型
	  * @param basePah 基础路径
	  * @param fileName 文件名
	  * @throws TemplateException
	  * @throws IOException
	  * @throws FileNotFoundException
	  * @author jjtEatJava
	  * @date 2017年8月8日
	  */
	 private void createFile(Template temp, Map<String, Object> model,String basePah, String fileName)
			throws TemplateException, IOException, FileNotFoundException {
		//创建文件夹
		File baseDir = new File(basePah);
		if(!baseDir.exists())
			baseDir.mkdirs();
		File file =new File (baseDir,fileName);
		if(file.exists()) {
			file.delete();
			file.createNewFile();
		}else {
			file.createNewFile();
		}
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Writer writer = new OutputStreamWriter(out);
		//把模版加载到内存
		temp.process(model,writer);
		//获取缓存字符数组
		byte[] byteBuff = out.toByteArray();
		RandomAccessFile fileOutput= new RandomAccessFile(file,"rw");
		FileChannel chn =fileOutput.getChannel();
		//隧道 映射
		MappedByteBuffer buff = chn.map(FileChannel.MapMode.READ_WRITE, 0,byteBuff.length);
		for(int i=0;i<byteBuff.length;i++) {
			buff.put(byteBuff[i]);
		}
		out.close();
		fileOutput.close();
	}
	
	 /**
	  * 
	  * Title:子类实现方法
	  * Description:
	  * @return
	  * @author jjtEatJava
	 * @throws TemplateException 
	 * @throws IOException 
	 * @throws ParseException 
	 * @throws MalformedTemplateNameException 
	 * @throws TemplateNotFoundException 
	 * @throws SQLException 
	  * @date 2017年8月8日
	  */
	 abstract public boolean build() throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, TemplateException, SQLException;
}
