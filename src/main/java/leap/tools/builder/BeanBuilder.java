package leap.tools.builder;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import freemarker.core.ParseException;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;
import leap.tools.bean.Field;
import leap.tools.config.JDBCConfig;
import leap.tools.logic.SimpleService;
import leap.tools.util.CamelChangeUtil;
import leap.tools.util.JDKPackUtil;
import leap.tools.util.ReadDirectory;

/**
 * Title: Description:
 * 
 * @author jjtEatJava
 * @date 2017年7月30日
 */
public class BeanBuilder extends Builder {
	private String packageName;
	private String basePath;
	private SimpleService service;
	private String mod;

	public BeanBuilder(JDBCConfig jdbcConfig, String basePath, String packageName,String mod) {
		this.packageName = packageName;
		this.basePath = new File(basePath, "leap").getAbsolutePath();
		this.mod = mod;
		service = new SimpleService(jdbcConfig);
	}

	/*
	 * （非 Javadoc）
	 * 
	 * @see com.kiki.jjt.freemark.Builder#build()
	 */
	@Override
	public boolean build() throws TemplateNotFoundException, MalformedTemplateNameException, ParseException,
			IOException, TemplateException, SQLException {
		// 配置包名
		FreeMarkService.getCfg().setSharedVariable("packageName", packageName);
		// 选择模版
		String templeName = "Bean.ftl";
		// 导入的包
		// 所有属性
		List<String> tablesName = service.getTablesName();
		for (int i = 0; i < tablesName.size(); i++) {
			String entityName = CamelChangeUtil.toCamel(tablesName.get(i),mod);
			List<Field> fields = service.getFieldsByTableName(tablesName.get(i),mod);
			Set<String> importPacks = JDKPackUtil.getPackByFields(fields);
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("fields", fields);
			model.put("importPacks", importPacks);
			model.put("entityName", entityName);
			model.put("tableName", tablesName.get(i));
			work(templeName, model, new File(basePath, "bean").getAbsolutePath(), entityName + ".java");
		}
		// 打印树形结构目录
		ReadDirectory rd = new ReadDirectory();
		rd.printDir(basePath);
		rd.readFile(basePath);
		return true;
	}
}
