package leap.tools.bean;

/**
 * Title:数据库类型与java类型的相互映射bean
 * Description:
 * @author jjtEatJava
 * @date 2017年7月30日
 */
public class Field {

	private String tableField;//数据库字段名
	private String tableType;//数据库类型
	private String tableKey;//数据库是否是主要关键字段
	
	private String javaField;//java字段名
	private String javaType;//java类型
	public String getTableField() {
		return tableField;
	}
	public void setTableField(String tableField) {
		this.tableField = tableField;
	}
	public String getTableType() {
		return tableType;
	}
	public void setTableType(String tableType) {
		this.tableType = tableType;
	}
	public String getJavaField() {
		return javaField;
	}
	public void setJavaField(String javaField) {
		this.javaField = javaField;
	}
	public String getJavaType() {
		return javaType;
	}
	public void setJavaType(String javaType) {
		this.javaType = javaType;
	}
	public String getTableKey() {
		return tableKey;
	}
	public void setTableKey(String tableKey) {
		this.tableKey = tableKey;
	}
	
	
}
