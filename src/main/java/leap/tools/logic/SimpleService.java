/**
 * Title:
 * Description:
 * @author jjtEatJava
 * @date 2017年7月30日
 */
package leap.tools.logic;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import leap.tools.bean.Field;
import leap.tools.config.JDBCConfig;
import leap.tools.util.CamelChangeUtil;
import leap.tools.util.TypeChangeUtil;
/**
 * Title:
 * Description:
 * @author jjtEatJava
 * @date 2017年7月30日
 */
public class SimpleService {
	
	
	public SimpleDao dao = null;
	
	public SimpleService(JDBCConfig config) {
		this.dao = new SimpleDao(config);
	}
	/**
	 * 
	 * Title:获取所有表名
	 * Description:
	 * @return
	 * @throws SQLException
	 * @author jjtEatJava
	 * @date 2017年8月8日
	 */
	public  List<String> getTablesName() throws SQLException{
		return this.dao.getAllTableName();
	}
	
	/**
	 * 
	 * Title:根据切割符号，获取所有实体名，即数据库表名与java类名的转换
	 * Description:
	 * @param mod 切割符号
	 * @return
	 * @throws SQLException
	 * @author jjtEatJava
	 * @date 2017年8月8日
	 */
	public  List<String> getEntitysName(String mod) throws SQLException {
		List<String> tablesName =  this.dao.getAllTableName();
		List<String> entitysName =new ArrayList<String>();
		for(String tableName:tablesName) {
			entitysName.add(CamelChangeUtil.toCamel(tableName, mod));
		}
		return entitysName;
	}

	/**
	 * 
	 * Title:根据表名与切割符号获取表里面所有的字段描述
	 * Description:
	 * @param tableName
	 * @param mod
	 * @return
	 * @throws SQLException
	 * @author jjtEatJava
	 * @date 2017年8月8日
	 */
	public  List<Field> getFieldsByTableName(String tableName,String mod) throws SQLException {
		//获取表里面所有数据库描述
		List<Field> field = this.dao.getAllFieldsByTableName(tableName);
		for(Field f : field) {
			String javaField = CamelChangeUtil.toCamel(f.getTableField(), mod);
			f.setJavaField(javaField);
			String javaType = TypeChangeUtil.tableTypeToJavaType(f.getTableType());
			f.setJavaType(javaType);	
		}
		return field;
	}
}
