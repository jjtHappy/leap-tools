package leap.tools.util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import leap.tools.bean.Field;

/**
 * Title:
 * Description:
 * @author jjtEatJava
 * @date 2017年8月8日
 */
public class JDKPackUtil {
	
	/**
	 * 
	 * Title:根据所有的域获取导包列表
	 * Description:
	 * @param fields bean里面的所有的域
	 * @return
	 * @author jjtEatJava
	 * @date 2017年8月8日
	 */
	public  static Set<String> getPackByFields(List<Field> fields) {
		Set<String> set = new HashSet<String>();
		for(Field field:fields) {
			String javaType = field.getJavaType();
			String packageName=null;
			switch(javaType) {
				case "String":
					packageName="java.lang.String";
					break;
				case "Integer":
					packageName="java.lang.Integer";
					break;
				case "Double":
					packageName="java.lang.Double";
					break;
				case "Float":
					packageName="";
					break;
				case "byte[]":
					packageName="";
					break;
				case "Date":
					packageName="java.util.Date";
					break;
				case "BigDecimal":
					packageName="java.math.BigDecimal";
					break;
				default :
				packageName="";
			}
			set.add(packageName);
		}
		return set;
	}
}
