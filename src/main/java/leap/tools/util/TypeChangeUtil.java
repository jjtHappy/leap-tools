package leap.tools.util;

/**
 * Title:数据库类型转换工具
 * Description:
 * @author jjtEatJava
 * @date 2017年8月8日
 */
public class TypeChangeUtil {
	/**
	 * Title:将数据库类型转为java类型
	 * Description:
	 * @param tableType 数据库类型
	 * @return
	 * @author jjtEatJava
	 * @date 2017年7月30日
	 */
	public static String tableTypeToJavaType(String tableType) {
		//去括号
		int lastIndex = tableType.lastIndexOf("(");
		tableType = tableType.substring(0, lastIndex==-1?tableType.length():lastIndex).toLowerCase();
		String javaType;
		switch(tableType) {
			case	"char":
			case	"varchar":
			case	"tinytext":
			case	"text":
			case	"mediumtext":
			case	"longtext":
				javaType="String";
				break;
			case	"tinyblob":
			case	"blob":
			case	"mediumblob":
			case	"longblob":
				javaType="byte[]";
				break;		
			case	"tinyint":
			case	"smallint":
			case	"mediumint":
			case	"int":
			case	"bigint":
				javaType="Integer";
				break;
			case	"float":
				javaType="float";
				break;
			case	"double":
				javaType="double";
				break;
			case	"decimal":
				javaType="BigDecimal";
				break;
			case	"date":
			case	"time":
			case	"year":
			case	"datetime":
			case	"timestamp":
				javaType="Date";
				break;
			default:
				throw new RuntimeException("无法解析"+tableType+"类型");
		}
		return javaType;
	}
}
