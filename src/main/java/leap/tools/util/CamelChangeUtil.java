/**
 * Title:
 * Description:
 * @author jjtEatJava
 * @date 2017年7月30日
 */
package leap.tools.util;

/**
 * Title:驼峰转换工具
 * Description:
 * @author jjtEatJava
 * @date 2017年7月30日
 */
public class CamelChangeUtil {
	/**
	 * 
	 * Title:把传入的字段转为驼峰
	 * Description:
	 * @param table 传入的字段
	 * @param mod 切割字符
	 * @return
	 * @author jjtEatJava
	 * @date 2017年8月8日
	 */
	public static String toCamel(String string,String mod) {
		String[] tableNameSplit = string.split(mod);
		StringBuffer entityName = new StringBuffer();
		for(String s : tableNameSplit) {
			char[] cs=s.toLowerCase().toCharArray();
	        cs[0]-=32;
	        entityName.append(String.valueOf(cs));
		}
		return entityName.toString();
	}
}
