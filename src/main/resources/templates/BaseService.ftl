package ${packageName}.service;
import java.io.Serializable;
import leap.orm.dao.Dao;

public interface BaseService <T,B extends Serializable>{
	Dao dao=Dao.get();
	
	T findById(B id);

	int insert(T entity);
	
	int update(T entity);
	
	int deleteById(B id);
	
	Dao getDao();
}
