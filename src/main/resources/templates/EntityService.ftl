package ${packageName}.service;

import ${packageName}.bean.${entityName};


public interface ${entityName}Service extends BaseService<${entityName},${priType}> {

}
