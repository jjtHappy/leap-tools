package ${packageName}.bean;

<#list importPacks as importPack>
import ${importPack};
</#list>
import leap.orm.annotation.Column;
import leap.orm.annotation.Id;
import leap.orm.annotation.Table;
import leap.orm.model.Model;

@Table("${tableName}")
public class ${entityName} extends Model implements java.io.Serializable {
	<#-- 构造属性 -->
	<#list fields as field>
	<#if field.tableKey?contains("PRI")>
	@Id
	</#if>
	@Column(name="${field.tableField}")
	private ${field.javaType} ${field.javaField?uncap_first};
	</#list>
	
	<#-- 构造set and get -->
	<#list fields as field >
	<#-- set -->
	public void set${field.javaField?cap_first}(${field.javaType} ${field.javaField?uncap_first}){
		this.${field.javaField?uncap_first}=${field.javaField?uncap_first};
	}
	<#-- get -->
	public ${field.javaType} get${field.javaField?cap_first}(){
		return this.${field.javaField?uncap_first};
	}
	</#list>
}