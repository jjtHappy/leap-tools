package ${packageName}.service.impl;
import java.io.Serializable;
import ${packageName}.service.BaseService;
import leap.orm.dao.Dao;

public abstract class BaseServiceImpl<T, B extends Serializable> implements BaseService<T, B> {
	
	@SuppressWarnings("unchecked")
	@Override
	public T findById(B id) {
		return (T)dao.findOrNull(getType(),id);
	}

	@Override
	public int insert(T entity){
		return dao.insert(entity);
	}
	
	@Override
	public int update(T entity){
		return dao.update(entity);
	}
	@Override
	public int deleteById(B id){
		return dao.delete(getType(),id);
	}
	
	@Override
	public  Dao getDao(){
		return dao;
	}
	
	protected abstract Class<T> getType();
	
}
