package ${packageName}.service.impl;

import ${packageName}.bean.${entityName};
import ${packageName}.service.${entityName}Service;
import leap.core.annotation.Bean;

 @Bean(id="${entityName?uncap_first}Service")
public class ${entityName}ServiceImpl extends BaseServiceImpl<${entityName},${priType}> implements ${entityName}Service{
	@Override
	protected  Class<${entityName}> getType(){
		return ${entityName}.class;
	};
}
