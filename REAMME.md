###  leap-tools


leap-tools是一款专属于leap(leapframework.org)的反向工程工具，能根据数据库文件自动生成leap要求的javabean与整个service层。


###  下载


使用git直接拉到本地，使用main方法调用即可

 

### 使用

以下面的例子为例：
![输入图片说明](https://git.oschina.net/uploads/images/2017/0808/171832_2a02f947_618906.png "屏幕截图.png")
我们需要根据上面的数据库文件创建javabean与service层，那么我们只需要

```
	MysqlConfig conf = new MysqlConfig("jdbc:mysql://localhost:3306/leap-tool-test", "root", "");//配置数据库驱动

//这里主要是使用了Mysql配置，如果你有别的数据源，你可以主动添加驱动文件，并创建JDBCConfig匿名内部类即可
        
        BeanBuilder builder = new BeanBuilder(conf, "H:\\test4", "leap.tool.test","_");//生成bean的主要构建者
	
        ServiceBuilder builder2 = new ServiceBuilder(conf, "H:\\test4", "leap.tool.test","_");//生成service层的主要构建者

//构建者的主要参赛是：数据库配置文件，基础生成路径，基础包名，命名时的分隔符
	
        builder.build();//构建
	
        builder2.build();//构建
```
生成的结果如下：
```
- H:\test4\leap
  - bean
    - LeapPost.java
    - LeapUser.java
    - LeapUserPost.java
  - service
    - BaseService.java
    - impl
      - BaseServiceImpl.java
      - LeapPostServiceImpl.java
      - LeapUserPostServiceImpl.java
      - LeapUserServiceImpl.java
    - LeapPostService.java
    - LeapUserPostService.java
    - LeapUserService.java
```

值得注意的几个点是：

1.工具会在你提供的基础路径下创建一个leap的文件夹来装所有生成的java文件

2.如果是javabean，那么会在leap文件夹下面创建bean文件夹装所有生成的java文件

3.如果是service层，那么会在leap文件夹下面创建service文件夹装所有生成的java文件,另外，会在service下创建impl文件夹创建所有的service层的impl java文件

以leap_user表为例，来看看生成的结果：

LeapUser:

```
package leap.tool.test.bean;

import java.util.Date;
import java.lang.String;
import java.lang.Integer;
import leap.orm.annotation.Column;
import leap.orm.annotation.Id;
import leap.orm.annotation.Table;
import leap.orm.model.Model;

@Table("leap_user")
public class LeapUser extends Model implements java.io.Serializable {
	@Id
	@Column(name="id")
	private String id;
	@Column(name="name")
	private String name;
	@Column(name="age")
	private Integer age;
	@Column(name="login_id")
	private String loginId;
	@Column(name="password")
	private String password;
	@Column(name="create_at")
	private Date createAt;
	
	public void setId(String id){
		this.id=id;
	}
	public String getId(){
		return this.id;
	}
	public void setName(String name){
		this.name=name;
	}
	public String getName(){
		return this.name;
	}
	public void setAge(Integer age){
		this.age=age;
	}
	public Integer getAge(){
		return this.age;
	}
	public void setLoginId(String loginId){
		this.loginId=loginId;
	}
	public String getLoginId(){
		return this.loginId;
	}
	public void setPassword(String password){
		this.password=password;
	}
	public String getPassword(){
		return this.password;
	}
	public void setCreateAt(Date createAt){
		this.createAt=createAt;
	}
	public Date getCreateAt(){
		return this.createAt;
	}
}
```

值得注意的点是：
包名：会在你给的基础包名基础上加上".bean"的包

BaseService:

```
package leap.tool.test.service;
import java.io.Serializable;
import leap.orm.dao.Dao;

public interface BaseService <T,B extends Serializable>{
	Dao dao=Dao.get();
	
	T findById(B id);

	int insert(T entity);
	
	int update(T entity);
	
	int deleteById(B id);
	
	Dao getDao();
}

```
值得注意的点是：
包名：会在你给的基础包名基础上加上".service"的包

主要定义了Service的通用方法

BaseServiceImpl

```
package leap.tool.test.service.impl;
import java.io.Serializable;
import leap.tool.test.service.BaseService;
import leap.orm.dao.Dao;

public abstract class BaseServiceImpl<T, B extends Serializable> implements BaseService<T, B> {
	
	@SuppressWarnings("unchecked")
	@Override
	public T findById(B id) {
		return (T)dao.findOrNull(getType(),id);
	}

	@Override
	public int insert(T entity){
		return dao.insert(entity);
	}
	
	@Override
	public int update(T entity){
		return dao.update(entity);
	}
	@Override
	public int deleteById(B id){
		return dao.delete(getType(),id);
	}
	
	@Override
	public  Dao getDao(){
		return dao;
	}
	
	protected abstract Class<T> getType();
	
}

```

对BaseService接口进行实现，但是还未泛化
值得注意的点是：
包名：会在你给的基础包名基础上加上".service.impl"的包

LeapUserService:

```
package leap.tool.test.service;

import leap.tool.test.bean.LeapUser;

public interface LeapUserService extends BaseService<LeapUser,String> {

}

```

值得注意的点是：
包名：会在你给的基础包名基础上加上".service"的包

泛化了BaseService接口，定义了具体实体的实体类与主键类型

LeapUserServiceImpl

```
package leap.tool.test.service.impl;

import leap.tool.test.bean.LeapUser;
import leap.tool.test.service.LeapUserService;
import leap.core.annotation.Bean;

 @Bean(id="leapUserService")
public class LeapUserServiceImpl extends BaseServiceImpl<LeapUser,String> implements LeapUserService{
	@Override
	protected  Class<LeapUser> getType(){
		return LeapUser.class;
	};
}

```
值得注意的点是：
包名：会在你给的基础包名基础上加上".service.impl"的包
实体服务的最终实现